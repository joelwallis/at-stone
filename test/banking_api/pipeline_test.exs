defmodule BankingApiTest.PipelineTest do
  use ExUnit.Case, async: true
  use Plug.Test

  @opts BankingApi.Pipeline.init([])

  test "Welcome message on root path" do
    conn =
      conn(:get, "/")
      |> BankingApi.Pipeline.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "Welcome to the Banking API"
  end

  test "404 Not Found error elsewhere" do
    Enum.each(["/foo", "/bar", "/baz"], fn path ->
      conn =
        conn(:get, path)
        |> BankingApi.Pipeline.call(@opts)

      assert conn.state == :sent
      assert conn.status == 404
      assert conn.resp_body == "Not found"
    end)
  end
end
