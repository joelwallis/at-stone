defmodule BankingApi.Router.Welcome do
  use Plug.Router

  plug(:match)
  plug(:dispatch)

  get "/" do
    conn
    |> put_resp_content_type("text/plain")
    |> send_resp(200, "Welcome to the Banking API")
    |> Plug.Conn.halt()
  end

  match(_, do: conn)
end
