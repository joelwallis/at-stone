defmodule BankingApi.Pipeline do
  use Plug.Builder

  plug(Plug.Logger)
  plug(BankingApi.Router.Welcome)
  plug(BankingApi.Router.NotFound)
end
