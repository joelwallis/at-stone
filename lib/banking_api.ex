defmodule BankingApi do
  use Application
  require Logger

  def start(_type, _args) do
    Logger.info("Starting Banking API")

    children = [{Plug.Cowboy, scheme: :http, plug: BankingApi.Pipeline, options: [port: 8080]}]

    Supervisor.start_link(children, strategy: :one_for_one, name: BankingApi.Supervisor)
  end
end
